import java.util.*;

public class NatureGenerator{
    static ArrayList<Double> generate(){
        ArrayList<Double> nature = new ArrayList<Double>();
        if (Math.random() * 2 < 1) {
          nature.add(1.2);
          nature.add(1.0);
          nature.add(1.0);
          nature.add(1.0);
          nature.add(0.8);
          Collections.shuffle(nature);
        }
        else {
          nature.add(1.0);
          nature.add(1.0);
          nature.add(1.0);
          nature.add(1.0);
          nature.add(1.0);
        }

        return nature;
    }
}
