import java.util.*;

public class IvGenerator {
    static ArrayList<Integer> generate(){
        ArrayList<Integer> ivs = new ArrayList<Integer>();
        Random r = new Random();
        ivs.add(r.nextInt(31-0) + 0);
        ivs.add(r.nextInt(31-0) + 0);
        ivs.add(r.nextInt(31-0) + 0);
        ivs.add(r.nextInt(31-0) + 0);
        ivs.add(r.nextInt(31-0) + 0);
        ivs.add(r.nextInt(31-0) + 0);
        return ivs;
    }
}