import java.util.*;
import Pokemon.Charmander;
import Pokemon.Charmeleon;
import Pokemon.Pokemon;

public class PokemonGenerator{
    static Pokemon generate(){
        ArrayList<Integer> ivs = IvGenerator.generate();
        ArrayList<Double> nature = NatureGenerator.generate();

        double r = Math.random() * ( 2 - 1 );
        if (r<0.5){
            return new Charmander(ivs,nature,1,65);

        }
        else {
            return new Charmeleon(ivs,nature,1,1040);

        }
    }
}