import java.util.ArrayList;
import Pokemon.Pokemon;
import java.util.Scanner;
import Pokemon.HiddenPower;
public class Game{
    public static void main(String[] args){
        Pokemon pokemon = PokemonGenerator.generate();
        System.out.println("Welcome to the world of pokemon");
        Scanner in = new Scanner(System.in);
        System.out.println("Visit https://bulbapedia.bulbagarden.net/wiki/Nature for more info on natures");
        System.out.println("Visit https://bulbapedia.bulbagarden.net/wiki/Individual_values for more info on ivs");
        System.out.println("Visit https://bulbapedia.bulbagarden.net/w/index.php?title=Hidden_power_calculation&redirect=no for more info on hiddenpower calculation");
try {
  long startTime = System.currentTimeMillis();
long maxDurationInMilliseconds = 30 * 60 * 1000;

    while (System.currentTimeMillis() < startTime + maxDurationInMilliseconds){

        System.out.println("Choose what you want to do: \n 1) Feed Rare Candies to level up \n 2) Print pokemon name \n 3) show level \n 4) print stats \n 5) print ivs and nature and hiddenPower \n 6) quit");
        int a = in.nextInt();
        if (a==6){
            break;
        }
        if (a == 5){
            System.out.println(pokemon.getIvs());
            ArrayList ivs = pokemon.getIvs();
            int b = (Integer) ivs.get(0) % 2;
            int c = (Integer) ivs.get(1) % 2;
            int d = (Integer) ivs.get(2) % 2;
            int f = (Integer) ivs.get(3) % 2;
            int g = (Integer) ivs.get(4) % 2;

            int id = (b + c *  2 + d * 4 + f * 8 + g * 16 + b * 32 ) * 15 / 63;
            System.out.println(HiddenPower.values()[id]);
            System.out.println(Nature.read(pokemon.getNature(),b+c));
            System.out.println(pokemon.getEvs());

            System.out.println(pokemon.getNature());

        }
       if (a == 4){
            System.out.println(pokemon.calchp());
            System.out.println(pokemon.calcstat(pokemon.attack,(Integer)pokemon.getIvs().get(1),(Double)pokemon.getNature().get(1)));
            System.out.println(pokemon.calcstat(pokemon.def,(Integer)pokemon.getIvs().get(2),(Double)pokemon.getNature().get(1)));
            System.out.println(pokemon.calcstat(pokemon.spa,(Integer)pokemon.getIvs().get(3),(Double)pokemon.getNature().get(2)));
            System.out.println(pokemon.calcstat(pokemon.spd,(Integer)pokemon.getIvs().get(4),(Double)pokemon.getNature().get(3)));
            System.out.println(pokemon.calcstat(pokemon.spe,(Integer)pokemon.getIvs().get(5),(Double)pokemon.getNature().get(4)));

        }

        if (a == 3){
            System.out.println("level: " + pokemon.getExp() / pokemon.getExprate());

        }
        if (a==1){
            System.out.println("How many?");
            int b = in.nextInt();
            pokemon = pokemon.levelup(b);

        }
        if (a==2){
            System.out.println(pokemon.getName());

        }

    }

}
catch(Exception e) {
}

       /* ArrayList<Integer> ivs = new ArrayList<Integer>();
        ivs.add(30);
        ivs.add(30);
        ivs.add(30);
        ivs.add(30);
        ivs.add(30);
        ivs.add(30);
        ArrayList<Double> nature = new ArrayList<Double>();
        nature.add(1.2);
        nature.add(1.0);
        nature.add(1.0);
        nature.add(1.0);
        nature.add(0.8);
        Pokemon pokemon = new Charmander(ivs,nature,1,65);
        pokemon.setName("Charmander");
        System.out.println(pokemon.getName());

        System.out.println(pokemon.calchp());
        System.out.println(pokemon.calcstat(pokemon.attack,(Integer)ivs.get(1),(Double)Pokemon.nature.get(1)));
        System.out.println(pokemon.calcstat(pokemon.def,(Integer)ivs.get(2),(Double)Pokemon.nature.get(1)));
        System.out.println(pokemon.calcstat(pokemon.spa,(Integer)ivs.get(3),(Double)Pokemon.nature.get(2)));
        System.out.println(pokemon.calcstat(pokemon.spd,(Integer)ivs.get(4),(Double)Pokemon.nature.get(3)));
        System.out.println(pokemon.calcstat(pokemon.spe,(Integer)ivs.get(5),(Double)Pokemon.nature.get(4)));
        Pokemon leveldup = pokemon.levelup(16);
        System.out.println(leveldup.getName());
        System.out.println(leveldup.calchp());
        System.out.println(leveldup.calcstat(leveldup.attack,(Integer)ivs.get(1),(Double)Pokemon.nature.get(1)));
        System.out.println(leveldup.calcstat(leveldup.def,(Integer)ivs.get(2),(Double)Pokemon.nature.get(1)));
        System.out.println(leveldup.calcstat(leveldup.spa,(Integer)ivs.get(3),(Double)Pokemon.nature.get(2)));
        System.out.println(leveldup.calcstat(leveldup.spd,(Integer)ivs.get(4),(Double)Pokemon.nature.get(3)));
        System.out.println(leveldup.calcstat(leveldup.spe,(Integer)ivs.get(5),(Double)Pokemon.nature.get(4)));
        Pokemon leveldup2 = leveldup.levelup(36);
        System.out.println(leveldup2.getName());

        */

    }
}
