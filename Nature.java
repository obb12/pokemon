import java.util.*;
import Pokemon.Pokemon;
class Nature{
  static String read(ArrayList n,int a){
    if ((Double)n.get(0) == 1.2) {
        if ((Double)n.get(1) == 0.8) {
          return "Lonely";

        }
        else if ((Double)n.get(2) == 0.8) {
          return "Adamant";

        }
        else if ((Double)n.get(3) == 0.8) {
          return "Naughty";
        }
        else {
          return "Brave";
        }

    }
    else if ((Double)n.get(1) == 1.2) {
      if ((Double)n.get(0) == 0.8) {
        return "Bold";

      }
      else if ((Double)n.get(2) == 0.8) {
        return "Impish";

      }
      else if ((Double)n.get(3) == 0.8) {
        return "Lax";
      }
      else {
        return "Relaxed";
      }
    }
    else if ((Double)n.get(2) == 1.2) {
      if ((Double)n.get(1) == 0.8) {
        return "Modest";

      }
      else if ((Double)n.get(3) == 0.8) {
        return "Mild";

      }
      else if ((Double)n.get(4) == 0.8) {
        return "Quiet";
      }
      else {
        return "Rash";
      }
    }
    else if ((Double)n.get(3) == 1.2) {
      if ((Double)n.get(1) == 0.8) {
        return "Calm";

      }
      else if ((Double)n.get(2) == 0.8) {
        return "Gentle";

      }
      else if ((Double)n.get(4) == 0.8) {
        return "Careful";
      }
      else {
        return "Sassy";
      }
    }
    else if ((Double)n.get(4) == 1.2) {
      if ((Double)n.get(1) == 0.8) {
        return "Timid";

      }
      else if ((Double)n.get(2) == 0.8) {
        return "Hasty";

      }
      else if ((Double)n.get(3) == 0.8) {
        return "Jolly";
      }
      else {
        return "Naive";
      }
    }
    else{
      Random rand = new Random(a);
   int index = rand.nextInt(5);
    return index == 0  ? "Hardy" : index == 1 ? "Docile" : index == 2 ? "Bashful": index == 3 ? "Quirky" : "Serious";

    }
  }
}
