package Pokemon;

import java.util.ArrayList;
public class Charmander extends    Pokemon {
    public Charmander(ArrayList ivs,ArrayList nature,int ability,int exp){
    setName("Charmander");
        setExprate(65);
        setExp(exp);
        setIvs(ivs);
        setNature(nature);
        setAbility(ability);
        setAttack(52);
        setDef(43);
        setHp(39);
        setSpa(60);
        setSpd(50);
        setSpe(65);
    }
    public int hp = 39;
    public int attack = 52;
    public int def = 43;
    public int spa = 60;
    public int spd = 50;
    public int spe = 65;
    public int accuracy;
    
    public double calchp (){
        return Math.floor((2 * hp + 31 + exp ) * (1) / 100 + (1) + 10);

    }

    public double calcstat (int stat,int iv,double nature){
        return Math.floor(Math.floor((2 * stat + iv + exp) * (1) / 100 +5) * nature);

    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }
    public int evasion;

    public int getEvasion() {
        return evasion;
    }

    public void setEvasion(int evasion) {
        this.evasion = evasion;
    }
    public int critratio;
    public int getCritratio() {
        return critratio;
    }


    public void setCritratio(int critratio) {
        this.critratio = critratio;
    }

    public  int ability;

    public void setAbility(int ability) {
        this.ability = ability;
    }

    public int getAbility() {
        return ability;
    }

    public ArrayList nature;

    public void setNature(ArrayList nature) {
        this.nature = nature;
    }

    public ArrayList getNature() {
        return nature;
    }

    protected Pokemon evolotion(){
        return new Charmeleon(ivs,nature,ability,exp);
    }
    protected int evolotionat = 16;
    protected String name;
    public Pokemon levelup(int amount){
        setExp((exp) + amount * exprate);
        if (exp / exprate >= evolotionat){
            return new Charmeleon(ivs,nature,ability,exp);

        } else {
            return this;
        }
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected  ArrayList ivs;

    public ArrayList getIvs() {
        return ivs;
    }

    public void setIvs(ArrayList ivs) {
        this.ivs = ivs;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setSpa(int spa) {
        this.spa = spa;
    }

    public void setSpe(int spe) {
        this.spe = spe;
    }

    public void setSpd(int spd) {
        this.spd = spd;
    }
    private ArrayList evs;

    public ArrayList getEvs() {
        return evs;
    }

    public void setEvs(ArrayList evs) {
        this.evs = evs;
    }
    public  int exp;
    public int exprate;
    public  int level(){
        return exp / exprate;
    }
    public Item item;
    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getExprate() {
        return exprate;
    }

    public void setExprate(int exprate) {
        this.exprate = exprate;
    }
}
