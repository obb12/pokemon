package Pokemon;
import java.util.ArrayList;
public  class Pokemon{
    public int hp;
    public int attack;
    public int def;
    public int spa;
    public int spd;
    public int spe;
    public int getHiddenPower(){
      return 0;
    }
    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setSpa(int spa) {
        this.spa = spa;
    }

    public void setSpe(int spe) {
        this.spe = spe;
    }

    public void setSpd(int spd) {
        this.spd = spd;
    }

    public int getAttack() {
        return attack;
    }

    public int getDef() {
        return def;
    }

    public int getHp() {
        return hp;
    }

    public int getSpa() {
        return spa;
    }

    public int getSpd() {
        return spd;
    }

    public int getSpe() {
        return spe;
    }

    public int accuracy;
    protected String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected  ArrayList ivs;

    public ArrayList getIvs() {
        return ivs;
    }

    public void setIvs(ArrayList ivs) {
        this.ivs = ivs;
    }
    private ArrayList evs;

    public ArrayList getEvs() {
        return evs;
    }
    public ArrayList nature;
    public void setNature(ArrayList nature) {
        this.nature = nature;
    }

    public ArrayList getNature() {
        return nature;
    }
    public double calchp () {
        return 0;
    }

    public double calcstat(int a,int b,double c) {
        return 0;
    }
        public Pokemon levelup(int amount){
    return this;
    }
    public void setEvs(ArrayList evs) {
        this.evs = evs;
    }
    public  int exp;
    public int exprate;
    public  int level(){
        return exp * exprate;
    }
    public Item item;
    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getExprate() {
        return exprate;
    }

    public void setExprate(int exprate) {
        this.exprate = exprate;
    }
}
