package Pokemon;

import java.util.ArrayList;
public class MegaCharizardY extends    Pokemon {
    public MegaCharizardY(ArrayList ivs,ArrayList nature,int ability,int exp){
    setName("Mega Charizard Y");
        setExprate(65);
        setExp(exp);
        setIvs(ivs);
        setNature(nature);
        setAbility(ability);
        setHp(78);

        setAttack(104);
        setDef(78);
        setSpa(159);
        setSpd(115);
        setSpe(100);
    }


    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setSpa(int spa) {
        this.spa = spa;
    }

    public void setSpe(int spe) {
        this.spe = spe;
    }

    public void setSpd(int spd) {
        this.spd = spd;
    }
    public  int ability;

    public void setAbility(int ability) {
        this.ability = ability;
    }

    public int getAbility() {
        return ability;
    }

    public ArrayList nature;

    public void setNature(ArrayList nature) {
        this.nature = nature;
    }

    public ArrayList getNature() {
        return nature;
    }
    public double calchp (){
        return Math.floor((2 * hp + 31 + exp ) * (1) / 100 + (1) + 10);

    }

    public double calcstat (int stat,int iv,double nature){

        return Math.floor(Math.floor((2 * stat + iv + exp) * (1) / 100 +5) * nature);

    }

    public Pokemon levelup(int amount){
        setExp((exp) + amount * exprate);
        System.out.println(1);
        return this;
    }
    protected String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    protected  ArrayList ivs;

    public ArrayList getIvs() {
        return ivs;
    }

    public void setIvs(ArrayList ivs) {
        this.ivs = ivs;
    }

    private ArrayList evs;

    public ArrayList getEvs() {
        return evs;
    }

    public void setEvs(ArrayList evs) {
        this.evs = evs;
    }
    public  int exp;
    public int exprate;
    public  int level(){
        return exp * exprate;
    }
    public Item item;
    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getExprate() {
        return exprate;
    }

    public void setExprate(int exprate) {
        this.exprate = exprate;
    }
}
